# Jupiter Task

Greetings. This is an assignment for Jupiter.

## Setting Up

The Website Stack used in this App consists of:

-	Create React App Bootstrapping
-   React
-	Redux (For global state management accross the pages)
-	Redux Persist (For offline local storage)

Clone this repo using your terminal using git or download the zip of this repo and extract it to a location on your system:

```bash
git clone httpshttps://gitlab.com/deve-sh/jupiterassignment.git
cd jupiterassignment
```

Now in order to install all dependencies, run:

```bash
npm install
```

After this is done, just run the app using:

```bash
npm start
```

## Usage and Features.

- The app follows atomic design, I.E: It is split into `container` components that manage the flow and data, and return `presentational` components that only render the data present in the app. 

- Reusable components are stored in the `reusables` folder.

- The website is mobile optimised (Responsive) to a large extent. Sorry if you're trying to open this on a smart watch. :P

- Similarly, the CSS is stored modularly in SCSS Files in the `static` folder, and all variables such as colors and sizes are stored in the `static\variables` folder.


## Suggestions

Any bugs and suggestions you may find in this app are welcome. Just drop me a message or raise an issue.