import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react"; // Redux-persist.

import "./static/main.scss";

import App from "./App";

// Production requirements.
import { disableReactDevTools } from "./prod";

import { store, persistor } from "./app/store";

// REMOVE react dev tools for production build to avoid state manipulation by user
if (process.env.NODE_ENV === 'production')
    disableReactDevTools();

ReactDOM.render(
  <Provider store={store}>
  	<PersistGate persistor={persistor}>
	    <Router>
	      <App />
	    </Router>
	</PersistGate>
  </Provider>,
  document.getElementById("root")
);
