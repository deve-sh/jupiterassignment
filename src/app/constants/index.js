const appConstants = {
	APPNAME: "Jupiter Task",
	MAXINPUTLENGTH: 30
};

export default appConstants;