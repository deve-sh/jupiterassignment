export const SET_USER_DATA = "SET_USER_DATA";
export const SET_NOTIFICATIONS = "SET_NOTIFICATIONS";

// Task actions

export const SET_TASKS = "SET_TASKS";

// Sidenav Handlers
export const SET_ACTIVE_TAB = "SET_ACTIVE_TAB";
export const SET_ACTIVE_SUBTAB = "SET_ACTIVE_SUBTAB";

// All data at once
export const SET_ALL_DATA = "SET_ALL_DATA";