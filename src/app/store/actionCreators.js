import * as actions from "./actions";

export const setUserData = (userData) => {
	return { type: actions.SET_USER_DATA, userData };
}

export const setNotifications = (notificationSettings) => {
	return { type: actions.SET_NOTIFICATIONS, notificationSettings };
}

export const setTasks = (tasks) => {
	return { type: actions.SET_TASKS, tasks };
}

export const setActiveTab = (tab = 0, subTab = 0) => {
	return { type: actions.SET_ACTIVE_TAB, tab, subTab };
}

export const setActiveSubTask = (subTab) => {
	return { type: actions.SET_ACTIVE_SUBTAB, subTab };
}

export const setAllAtOnce = (allData) => {
	return { type: actions.SET_ALL_DATA, allData };
}