const initialState = {
	userData: null,
	notificationSettings: null,
	tasks: [],
	tab: 0,
	subTab: 0
};

export default initialState;