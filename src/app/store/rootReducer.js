import * as actions from "./actions";
import initialState from "./initialState";

const rootReducer = (state = initialState, action = { type: null }) => {
	switch (action.type) {
		case actions.SET_USER_DATA:
			return { ...state, userData: action.userData };
		case actions.SET_NOTIFICATIONS:
			return { ...state, notificationSettings: action.notificationSettings };
		case actions.SET_TASKS:
			return { ...state, tasks: action.tasks };
		case actions.SET_ACTIVE_TAB:
			return { ...state, tab: action.tab, subTab: action.subTab };
		case actions.SET_ACTIVE_SUBTAB:
			return { ...state, subTab: action.subTab };
		case actions.SET_ALL_DATA:
			return { ...state, ...action.allData };
		default:
			return state;
	}
};

export default rootReducer;