import React from "react";
import Switch from "react-switch";

import Button from "../reusables/Button";
import Dropdown from "../reusables/Dropdown";

// Icons
import ListIcon from "@material-ui/icons/List";

const TasksUI = (props) => {
	return (
		<div className={"settings"}>
			<div className={"settings-tabcontainer"}>
				<div className={"settings-tabcontainer-heading"}>
					{props.tasks && props.tasks.length > 0 ? (
						<React.Fragment>
							<div
								className={
									"settings-tabcontainer-heading-label"
								}
							>
								{props.tasks[props.activeSubTab]
									? props.tasks[props.activeSubTab].name
									: ""}
							</div>
							<div className={"settings-sectioncontainer"}>
								<form
									className={"settings-form"}
									onSubmit={props.taskSubmit}
								>
									<div className={"row inputrow"}>
										<div className={"col-6 labelcol"}>
											<div
												className={
													"settings-tabcontainer-heading-label inputlabel"
												}
											>
												Source Name
											</div>
										</div>
										<div className={"col-6 inputcol"}>
											<Dropdown
												className={
													"form-control taskdropdown"
												}
												options={
													props.taskInputs[
														"Source Options"
													]
												}
												name={"Source Name"}
												onChange={
													props.handleTaskChange
												}
												required={true}
												value={
													props.taskInputs[
														"Source Name"
													]
												}
											/>
										</div>
									</div>
									<div className={"row inputrow"}>
										<div className={"col-6 labelcol"}>
											<div
												className={
													"settings-tabcontainer-heading-label inputlabel"
												}
											>
												Enable Logging
											</div>
										</div>
										<div className={"col-6 inputcol"}>
											<Switch
												checked={
													props.taskInputs[
														"Enable Logging"
													]
												}
												onChange={(checked) =>
													props.handleLogSwitch(
														checked
													)
												}
												onColor={"#0588fd"}
											/>
										</div>
									</div>
									<div className={"row inputrow"}>
										<div className={"col-6 labelcol"}>
											<div
												className={
													"settings-tabcontainer-heading-label inputlabel"
												}
											>
												Provide SQL
											</div>
										</div>
										<div className={"col-6 inputcol"}>
											<textarea
												placeholder={
													"Provide SQL here."
												}
												onChange={
													props.handleTaskChange
												}
												className={
													"form-control sqlform"
												}
												required={true}
												value={props.taskInputs.SQL}
												name={"SQL"}
											/>
										</div>
									</div>
									<div className={"row inputrow"}>
										<div className={"col-6 labelcol"}>
											<div
												className={
													"settings-tabcontainer-heading-label inputlabel"
												}
											>
												Target Result
											</div>
										</div>
										<div className={"col-6 inputcol"}>
											<Dropdown
												className={
													"form-control taskdropdown"
												}
												options={
													props.taskInputs[
														"Target Options"
													]
												}
												name={"Target Result"}
												onChange={
													props.handleTaskChange
												}
												required={true}
												value={
													props.taskInputs[
														"Target Name"
													]
												}
											/>
										</div>
									</div>
									<div className={"buttoncontainer"}>
										<a
											href={"#"}
											className={"btn btn-danger"}
											onClick={props.resetProfileInputs}
										>
											Cancel
										</a>
										<Button
											type={"submit"}
											className={"btn btn-success"}
											label={"Submit"}
										/>
										<a
											href={"#"}
											className={"btn btn-primary"}
											onClick={props.validateInput}
										>
											Validate
										</a>
									</div>
								</form>
							</div>
						</React.Fragment>
					) : (
						<div className={"nonefound"}>
							<ListIcon />
							<div className={"nolistings-label"}>
								{"No Tasks Found."}
							</div>
						</div>
					)}
				</div>
			</div>
		</div>
	);
};

export default TasksUI;
