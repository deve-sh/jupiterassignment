import React from "react";

import { Link } from "react-router-dom";

const SidenavUI = (props) => {
	return (
		<React.Fragment>
			<div className={"sidenav"}>
				<div className={"sidenav-section"}>
					<div className={"sidenav-maintab"}>
						<Link
							to={"/"}
							onClick={(e) => {
								props.changeTab(0); // Change main tab.
							}}
							className={`sidenav-link mainlink ${
								props.activeTab === 0 ? "active" : ""
							}`}
						>
							Profile
						</Link>
					</div>
					<div className={"sidenav-subtabs"}>
						<Link
							to={"/"}
							onClick={(e) => {
								props.changeTab(0, 0); // Change main tab.
							}}
							className={`sidenav-link sublink ${
								props.activeSubTab === 0 && props.activeTab === 0 ? "active" : ""
							}`}
						>
							Settings
						</Link>
						<Link
							to={"/"}
							onClick={(e) => {
								props.changeTab(0, 1); // Change main tab.
							}}
							className={`sidenav-link sublink ${
								props.activeSubTab === 1 && props.activeTab === 0 ? "active" : ""
							}`}
						>
							Notifications
						</Link>
					</div>
				</div>
				<div className={"sidenav-section"}>
					<div className={"sidenav-maintab"}>
						<Link
							to={"/tasks"}
							onClick={(e) => {
								props.changeTab(1, 0); // Change main tab.
							}}
							className={`sidenav-link mainlink ${
								props.activeTab === 1 ? "active" : ""
							}`}
						>
							My Tasks
						</Link>
					</div>
					<div className={"sidenav-subtabs"}>
						{props.tasks &&
							props.tasks.length > 0 &&
							props.tasks.map((task, index) => (
								<Link
									to={"/tasks"}
									key={index}
									onClick={(e) => {
										props.changeTab(1, index); // Change main tab.
									}}
									className={`sidenav-link sublink ${
										props.activeSubTab === index && props.activeTab === 1
											? "active"
											: ""
									}`}
								>
									{task.name}
								</Link>
							))}
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default SidenavUI;
