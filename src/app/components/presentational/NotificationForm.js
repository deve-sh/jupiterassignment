import React from "react";
import Switch from "react-switch";

import Button from "../reusables/Button";

const NotificationForm = (props) => (
	<form className={"settings-form"} onSubmit={props.notificationSubmit}>
		{props.notificationSettings &&
			Object.keys(props.notificationSettings).length > 0 &&
			Object.keys(props.notificationSettings).map((setting, index) => (
				<div className={"row inputrow"} key={index}>
					<div className={"col-6 labelcol"}>
						<div
							className={
								"settings-tabcontainer-heading-label inputlabel"
							}
						>
							{setting}
						</div>
					</div>
					<div className={"col-6 inputcol"}>
						<Switch
							checked={props.notificationInputs[setting]}
							onChange={(checked) =>
								props.handleNotificationChange(checked, setting)
							}
							onColor={"#0588fd"}
						/>
					</div>
				</div>
			))}
		{props.notificationSettings &&
			Object.keys(props.notificationSettings).length > 0 && (
				<div className={"buttoncontainer"}>
					<a
						href={"#"}
						className={"btn btn-danger"}
						onClick={props.resetNotificationInputs}
					>
						Cancel
					</a>
					<Button
						type={"submit"}
						className={"btn btn-success"}
						label={"Submit"}
					/>
				</div>
			)}
	</form>
);

export default NotificationForm;
