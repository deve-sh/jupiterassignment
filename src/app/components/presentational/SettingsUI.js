import React from "react";

import Switch from "react-switch";

import constants from "../../constants";

// Nested Children Components
import ProfileEditingForm from "./ProfileEditingForm";
import NotificationForm from "./NotificationForm";

const SettingsUI = (props) => {
	return (
		<div className={"settings"}>
			<div className={"settings-tabcontainer"}>
				<div className={"settings-tabcontainer-heading"}>
					<div className={"settings-tabcontainer-heading-label"}>
						{props.activeSubTab === 0
							? "Profile Info"
							: "Notifications"}
					</div>
					<div className={"settings-sectioncontainer"}>
						{props.activeSubTab === 0 ? (
							<ProfileEditingForm {...props} />
						) : (
							<NotificationForm {...props} />
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default SettingsUI;
