import React from "react";

import Input from "../reusables/Input";
import Button from "../reusables/Button";

import { formatDate } from "../../helpers";

import constants from "../../constants";

const ProfileEditingForm = (props) => (
	<form className={"settings-form"} onSubmit={props.profileSubmit}>
		<div className={"row inputrow"}>
			<div className={"col-6 labelcol"}>
				<div
					className={"settings-tabcontainer-heading-label inputlabel"}
				>
					Name
				</div>
			</div>
			<div className={"col-6 inputcol"}>
				<Input
					type={"text"}
					maxLength={constants.MAXINPUTLENGTH}
					className={"form-control"}
					placeholder={"Name"}
					value={props.profileInputs.name}
					name={"name"}
					onChange={props.handleProfileInputChange}
					required={true}
				/>
			</div>
		</div>
		<div className={"row inputrow"}>
			<div className={"col-6 labelcol"}>
				<div
					className={"settings-tabcontainer-heading-label inputlabel"}
				>
					DOB
				</div>
			</div>
			<div className={"col-6 inputcol"}>
				<Input
					type={"date"}
					max={formatDate(
						new Date().setDate(new Date().getDate() - 1)
					)}
					className={"form-control"}
					placeholder={"dob"}
					value={props.profileInputs.dob}
					name={"dob"}
					onChange={props.handleProfileInputChange}
					required={true}
				/>
			</div>
		</div>
		<div className={"row inputrow"}>
			<div className={"col-6 labelcol"}>
				<div
					className={"settings-tabcontainer-heading-label inputlabel"}
				>
					Profession
				</div>
			</div>
			<div className={"col-6 inputcol"}>
				<Input
					type={"text"}
					maxLength={constants.MAXINPUTLENGTH}
					className={"form-control"}
					placeholder={"Profession"}
					value={props.profileInputs.profession}
					name={"profession"}
					onChange={props.handleProfileInputChange}
					required={true}
				/>
			</div>
		</div>
		<div className={"row inputrow"}>
			<div className={"col-6 labelcol"}>
				<div
					className={"settings-tabcontainer-heading-label inputlabel"}
				>
					Designation
				</div>
			</div>
			<div className={"col-6 inputcol"}>
				<Input
					type={"text"}
					maxLength={constants.MAXINPUTLENGTH}
					className={"form-control"}
					placeholder={"Designation"}
					value={props.profileInputs.designation}
					name={"designation"}
					onChange={props.handleProfileInputChange}
					required={true}
				/>
			</div>
		</div>
		<div className={"buttoncontainer"}>
			<a
				href={"#"}
				className={"btn btn-danger"}
				onClick={props.resetProfileInputs}
			>
				Cancel
			</a>
			<Button
				type={"submit"}
				className={"btn btn-success"}
				label={"Submit"}
			/>
		</div>
	</form>
);

export default ProfileEditingForm;
