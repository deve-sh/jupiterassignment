import React from 'react';

import PersonIcon from '@material-ui/icons/Person';

const HeaderUI = props => {
	return <header className={"header"}>
		<div className={"fixedcontainer row"}>
			<div className={"col-4 header-left"}></div>
			<div className={"col-8 header-right"}>
				<div className={"usercontainer"}>
					<PersonIcon />
					<div className={"username"}>
						{props.user ? props.user.name : ""}
					</div>
				</div>
			</div>
		</div>
	</header>
};

export default HeaderUI;