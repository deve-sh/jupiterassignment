import React, { useState } from "react";
import { useDispatch } from "react-redux";

import SettingsUI from "../presentational/SettingsUI";

import { setUserData, setNotifications } from "../../store/actionCreators";

import toasts from "../../constants/toastConstants";

const Settings = (props) => {
	const dispatch = useDispatch();

	const [profileInputs, setprofileInputs] = useState({
		...props.user,
	});

	const [notificationInputs, setnotificationInputs] = useState({
		...props.notificationSettings,
	});

	const profileSubmit = (event) => {
		event.preventDefault();

		// Validating Inputs

		if (
			!profileInputs.name.length ||
			profileInputs.name.length > 30 ||
			!profileInputs.profession.length ||
			profileInputs.profession.length > 30 ||
			!profileInputs.designation.length ||
			profileInputs.designation.length > 30 ||
			new Date(profileInputs.dob).getTime() > new Date().getTime()
		)
			return toasts.generateError("Invalid Inputs.");

		dispatch(setUserData(profileInputs));

		toasts.generateSuccess("Successfully updated profile.");
	};

	const handleProfileInputChange = (event) => {
		event.persist();
		setprofileInputs((inputs) => {
			return { ...inputs, [event.target.name]: event.target.value };
		});
	};

	const resetProfileInputs = (event) => {
		event.preventDefault();
		setprofileInputs({ ...props.user });
	};

	// Notification handlers

	const notificationSubmit = (event) => {
		event.preventDefault();
		dispatch(setNotifications(notificationInputs));
		toasts.generateSuccess("Updated notifications settings successfully.");
	};

	const handleNotificationChange = (checked, setting) => {
		setnotificationInputs((inputs) => {
			let newNotificationInputs = { ...inputs };
			// Logical validations
			for(let i in newNotificationInputs)
				if(i !== setting) newNotificationInputs[i] = false;
			toasts.generateWarning("Logical validation done.");
			return { ...newNotificationInputs, [setting]: checked };
		});
	};

	const resetNotificationInputs = (event) => {
		event.preventDefault();
		setnotificationInputs({ ...props.notificationSettings });
	}

	return (
		<SettingsUI
			activeTab={props.activeTab}
			activeSubTab={props.activeSubTab}
			user={props.user}
			notificationSettings={props.notificationSettings}
			// Profile handlers
			resetProfileInputs={resetProfileInputs}
			profileInputs={profileInputs}
			profileSubmit={profileSubmit}
			handleProfileInputChange={handleProfileInputChange}

			// Notification handlers
			notificationSubmit={notificationSubmit}
			notificationInputs={notificationInputs}
			handleNotificationChange={handleNotificationChange}
			resetNotificationInputs={resetNotificationInputs}
		/>
	);
};

export default Settings;
