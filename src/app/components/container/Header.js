import React from "react";

import HeaderUI from "../presentational/HeaderUI";

const Header = (props) => {
	return (
		<HeaderUI
			user={props.user}
		/>
	);
};

export default Header;
