import React from "react";

import SidenavUI from "../presentational/SidenavUI";

const Sidenav = (props) => {
	return (
		<SidenavUI
			activeTab={props.activeTab}
			activeSubTab={props.activeSubTab}
			tasks={props.tasks}
			changeTab={props.changeTab}
		/>
	);
};

export default Sidenav;
