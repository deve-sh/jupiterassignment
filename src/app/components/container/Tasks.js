import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import toasts from "../../constants/toastConstants";

import TasksUI from "../presentational/TasksUI";

import { setTasks } from "../../store/actionCreators";

const Tasks = (props) => {
	const dispatch = useDispatch();

	const [taskInputs, settaskInputs] = useState(
		props.tasks.length
			? {
					...props.tasks[props.activeSubTab],
			  }
			: {}
	);

	useEffect(() => {
		if (props.tasks.length)
			settaskInputs({
				...props.tasks[props.activeSubTab],
			});
	}, [props.activeSubTab]);

	// Task handlers

	const taskSubmit = (event) => {
		event.preventDefault();
		if (!taskInputs.SQL) return toasts.generateError("No SQL Entered.");

		let newTasks = [...props.tasks];
		newTasks[props.activeSubTab] = { ...taskInputs };

		dispatch(setTasks(newTasks));

		return toasts.generateSuccess("Successfully updated Tasks.");
	};

	const handleTaskChange = (event) => {
		event.persist();
		settaskInputs((inputs) => {
			return { ...inputs, [event.target.name]: event.target.value };
		});
	};

	const handleLogSwitch = (checked) => {
		settaskInputs((inputs) => {
			return { ...inputs, ["Enable Logging"]: checked };
		});
	};

	const validateInput = (event) => {
		event.preventDefault();

		// A basic check.
		if (!taskInputs.SQL) return toasts.generateError("No SQL Entered.");
		else return toasts.generateSuccess("All good.");
	};

	return (
		<TasksUI
			activeTab={props.activeTab}
			activeSubTab={props.activeSubTab}
			tasks={props.tasks}
			// Task form handlers
			taskInputs={taskInputs}
			taskSubmit={taskSubmit}
			handleTaskChange={handleTaskChange}
			handleLogSwitch={handleLogSwitch}
			validateInput={validateInput}
		/>
	);
};

export default Tasks;
