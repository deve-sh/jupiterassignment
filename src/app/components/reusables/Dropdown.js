import React from "react";

const Dropdown = props => {
  return (
    <select
      className={`select ${props.className ? props.className : ""}`}
      onChange={props.onChange}
      value={props.value}
      required={props.required}
      disabled={props.disabled}
      name={props.name}
    >
      {props.options
        ? props.options.map((option, index) => (
            <option key={index} value={option}>
              {option}
            </option>
          ))
        : ""}
    </select>
  );
};

export default Dropdown;
