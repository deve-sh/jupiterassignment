import React, { useState, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import Notifications from "react-notify-toast";

// The local JSON File for all the pre-set data for the app.
import dataJSON from "./data.json";

// Actions for store
import {
	setActiveTab,
	setActiveSubTab,
	setUserData,
	setNotifications,
	setTasks,
	setAllAtOnce,
} from "./app/store/actionCreators";

// Constants
import constants from "./app/constants";
import toasts from "./app/constants/toastConstants";

// Components
import Sidenav from "./app/components/container/Sidenav";
import Header from "./app/components/container/Header";
import Settings from "./app/components/container/Settings";
import Tasks from "./app/components/container/Tasks";

const App = (props) => {
	const dispatch = useDispatch();
	const state = useSelector((state) => state);

	const changeTab = (index = 0, subTabIndex) => {
		dispatch(setActiveTab(index, subTabIndex));
	};

	const changeSubTab = (index = 0) => {
		dispatch(setActiveTab(index));
	};

	useEffect(() => {
		if (
			!state.userData ||
			!state.notificationSettings ||
			!state.tasks.length ||
			state.tasks.length !== dataJSON.tasks.length
		) {
			// Load the JSON.
			dispatch(setAllAtOnce(dataJSON));
		}
	}, []);

	return (
		<React.Fragment>
			{/* Global Toast for error messages */}
			<Notifications />
			<Header user={state.userData} />
			<div className={"fixedcontainer row approw"}>
				<div className={"col-md-3 sidenavcol"}>
					<Sidenav
						activeTab={state.tab}
						activeSubTab={state.subTab}
						changeTab={changeTab}
						changeSubTab={changeSubTab}
						tasks={state.tasks}
					/>
				</div>
				<div className={"col-md-9 sectioncol"}>
					<Switch>
						<Route
							path={"/"}
							exact
							component={(props) => (
								<Settings
									{...props}
									activeTab={state.tab}
									activeSubTab={state.subTab}
									user={state.userData}
									notificationSettings={
										state.notificationSettings
									}
								/>
							)}
						/>
						<Route
							path={"/tasks"}
							component={(props) => (
								<Tasks
									{...props}
									activeTab={state.tab}
									activeSubTab={state.subTab}
									tasks={state.tasks}
								/>
							)}
						/>
					</Switch>
				</div>
			</div>
		</React.Fragment>
	);
};
export default App;
